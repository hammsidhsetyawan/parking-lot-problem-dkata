let maxSlot = 0
let Car = []
let availableSlot = []
let totalHours = 0

exports.createParkingLot = async (numSlot) => {
      try {
            maxSlot = parseInt(numSlot)
            for (let i = 1; i <= maxSlot; i++) {
                  availableSlot.push(i)
            }
            return `Created parking lot with ${availableSlot.length} slots`
      } catch (error) {
            return "Is not number"
      }
},
exports.park = async (registNo) => {
      if (maxSlot === Car.length) {
            return 'Sorry, parking lot is full'
      } else {
            let slot = availableSlot[0]
            Car.push({
                  'slot': slot,
                  'registNo': registNo
            })
            availableSlot.shift();
            return `Allocated slot number: ${slot}`
      }
},
exports.leave = async (registNo, hours) => {

      Array.prototype.remove = function () {
            var what, a = arguments, L = a.length, ax;
            while (L && this.length) {
                  what = a[--L];
                  while ((ax = this.indexOf(what)) !== -1) {
                  this.splice(ax, 1);
            }
            }
            return this;
      }
      try {
            totalHours = parseInt(hours)
            additionalHours = totalHours - 2
      if (totalHours <= 2) {
                  for( var i = 0; i < Car.length; i++){ 
                        if ( Car[i] === registNo) {
                              Car.splice(i, 1); 
                        }
                  }
                  return `Registration number ${registNo} with slot number is free with charge 10`
            } else if (totalHours > 2) {
                  for( var i = 0; i < Car.length; i++){ 
                        if ( Car[i] === registNo) {
                              Car.splice(i, 1); 
                        }
                     }
                  return `Registration number ${registNo} with slot number is free with charge ${additionalHours * 10 + 10} `
            } else {
                  return `Registration number ${registNo} not found`
            }
      } catch (error) {
            return error
      }

},
exports.status = async () => {
      try {
            if (Car.length > 0) {
            console.log(`Slot No.\tRegistration No.`)
                  Car.forEach(function (row) {
                        console.log(`${row.slot}\t\t${row.registNo}`)
                  })
            } else {
                  return "Parking lot is empty"
            }
      } catch (error) {
            return error
      }
}