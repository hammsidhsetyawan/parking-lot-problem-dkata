const readline = require('readline');
const fs = require('fs')
const parkingLotController = require('./controllers/parking')

// const output = fs.createWriteStream('./file_outputs.txt')
const readInterface = readline.createInterface({
      input: fs.createReadStream('./file_inputs.txt'),
      output: process.stdout
});

let main = () => {
      readInterface.on('line', async (input) => {
            input = input.split(" ")
            switch (input[0]) {
                  case ('create_parking_lot'):
                        try {
                              const result = await parkingLotController.createParkingLot(input[1])
                              console.log(result)
                        } catch (error) {
                              console.log(error)
                        }
                        break;

                  case ('park'):
                        try {
                              const result = await parkingLotController.park(input[1].trim())
                              console.log(result)
                        } catch (error) {
                              console.log(error)
                        }
                        break;
                  case ('leave'):
                        try {
                              const result = await parkingLotController.leave(input[1].trim(), input[2].trim())
                              console.log(result)
                        } catch (error) {
                              console.log(error)
                        }
                        break;
                  case ('status'):
                        try {
                              const result = await parkingLotController.status()
                              console.log(result)
                        } catch (error) {
                              console.log(error)
                        }
                        break;
            }
      })
}

// readInterface.on

main();