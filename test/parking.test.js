let parkingController = require ('../controllers/parking')
let expect = require('chai').expect

describe('Create Parking lot of size 6',async function(){
      it('create_parking_lot 6', async function(){
            var result = 'Created parking lot with 6 slots'
            var input = await parkingController.createParkingLot(6)

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-HH-1234',async function(){
      it('park KA-01-HH-1234', async function(){
            var result = 'Allocated slot number: 1'
            var input = await parkingController.park('KA-01-HH-1234')

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-HH-9999',async function(){
      it('park KA-01-HH-9999', async function(){
            var result = 'Allocated slot number: 2'
            var input = await parkingController.park('KA-01-HH-9999')

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-BB-0001',async function(){
      it('park KA-01-BB-0001', async function(){
            var result = 'Allocated slot number: 3'
            var input = await parkingController.park('KA-01-BB-0001')

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-HH-7777',async function(){
      it('park KA-01-HH-7777', async function(){
            var result = 'Allocated slot number: 4'
            var input = await parkingController.park('KA-01-HH-7777')

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-HH-2701',async function(){
      it('park KA-01-HH-2701', async function(){
            var result = 'Allocated slot number: 5'
            var input = await parkingController.park('KA-01-HH-2701')

            expect(input).to.be.equal(result)
      })
})

describe('Add Park KA-01-HH-3141',async function(){
      it('park KA-01-HH-3141', async function(){
            var result = 'Allocated slot number: 6'
            var input = await parkingController.park('KA-01-HH-3141')

            expect(input).to.be.equal(result)
      })
})

describe('Status',async function(){
      it('status', async function(){
            var result = 'Allocated slot number: 6'
            var input = await parkingController.status()

            expect(input).to.be.equal(input)
      })
})
      

